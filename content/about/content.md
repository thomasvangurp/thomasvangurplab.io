+++
fragment = "content"
#disabled = true
date = "2017-10-05"
weight = 100
#background = ""

title = "About Deena Bioinformatics"
#subtitle = ""
+++

---

Thomas is an experienced bioinformatician with over eleven years’ of experience working as a consultant for plant breeding companies and academic groups. He studied biology at Wageningen University, specializing in epigenetics and bioinformatics. He did his PhD at the Dutch institute of ecology where he invented a new method (epiGBS – Nature Methods) to study population level epigenetic variation in plant species.

Thomas is an expert in deploying, modifying and optimizing sequencing methods (Illumina/Nanopore) and their analysis. He is a proficient at developing process oriented and automated workflows using devops principles using workflow orchestration tools like airflow and snakemake. He is an expert Python programmer, highly experienced in developing (custom) visualization tracks for genome browsers such as Jbrowse and IGV. For the past 5 years, he has gained experience as a group lead of data scientist and bioinformaticians. As a person, Thomas is curious, creative and pragmatic. 
